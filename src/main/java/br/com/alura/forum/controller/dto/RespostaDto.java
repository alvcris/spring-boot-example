package br.com.alura.forum.controller.dto;

import br.com.alura.forum.modelo.Resposta;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RespostaDto {

    private Long id;
    private String message;
    private LocalDateTime createdDate;
    private String author;

    public RespostaDto(final Resposta resposta) {
        this.id = resposta.getId();
        this.message = resposta.getMensagem();
        this.createdDate = resposta.getDataCriacao();
        this.author = resposta.getAutor().getNome();
    }
}
