package br.com.alura.forum.controller.form;

import br.com.alura.forum.modelo.Curso;
import br.com.alura.forum.modelo.Topico;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TopicoForm extends TopicoBasicForm {

    private String NomeCurso;

    public Topico converter(final Curso curso) {
        return new Topico(titulo, mensagem, curso);

    }
}
