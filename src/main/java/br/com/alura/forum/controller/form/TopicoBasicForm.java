package br.com.alura.forum.controller.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TopicoBasicForm {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    protected String titulo;

    @NotNull
    @NotEmpty
    @Length(min = 5)
    protected String mensagem;
}
