package br.com.alura.forum.controller.dto;

import br.com.alura.forum.modelo.StatusTopico;
import br.com.alura.forum.modelo.Topico;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TopicoDtoDetails extends TopicoDto {

    List<RespostaDto> respostasDto;
    private String author;
    private StatusTopico statusTopico;

    public TopicoDtoDetails(final Topico topico) {
        this.id = topico.getId();
        this.titulo = topico.getTitulo();
        this.mensagem = topico.getMensagem();
        this.dataCriacao = topico.getDataCriacao();
        this.author = topico.getAutor().getNome();
        this.statusTopico = topico.getStatus();

        this.respostasDto = new ArrayList<>();
        this.respostasDto.addAll(
                topico.getRespostas()
                        .stream()
                        .map(RespostaDto::new)
                        .collect(Collectors.toList()));
    }


}
