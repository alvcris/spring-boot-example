package br.com.alura.forum.controller.dto;

import br.com.alura.forum.modelo.Topico;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TopicoDto {
    protected Long id;
    protected String titulo;
    protected String mensagem;
    protected LocalDateTime dataCriacao;

    public TopicoDto(final Topico topico) {
        this.id = topico.getId();
        this.titulo = topico.getTitulo();
        this.mensagem = topico.getMensagem();
        this.dataCriacao = topico.getDataCriacao();
    }

    public static Page<TopicoDto> converter(final Page<Topico> listaTopicos) {
        return listaTopicos.map(TopicoDto::new);
    }
}
