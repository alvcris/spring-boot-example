package br.com.alura.forum.controller;

import br.com.alura.forum.controller.dto.TopicoDto;
import br.com.alura.forum.controller.dto.TopicoDtoDetails;
import br.com.alura.forum.controller.form.TopicoBasicForm;
import br.com.alura.forum.controller.form.TopicoForm;
import br.com.alura.forum.modelo.Topico;
import br.com.alura.forum.service.CursoService;
import br.com.alura.forum.service.TopicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/topicos")
public class TopicosController {

    @Autowired
    TopicoService topicoService;

    @Autowired
    CursoService cursoService;

    @GetMapping
    public Page<TopicoDto> getAllTopics(Pageable pageable) {
        Page<Topico> topicos = topicoService.getAllTopics(pageable);

        return TopicoDto.converter(topicos);
    }

    @PostMapping
    public ResponseEntity<TopicoDto> createTopico(@RequestBody @Valid TopicoForm form, final UriComponentsBuilder uriComponentsBuilder) {
        Topico topico = form.converter(cursoService.findByNome(form.getNomeCurso()));
        topicoService.save(topico);

        URI uri = uriComponentsBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();
        return ResponseEntity.created(uri).body(new TopicoDto(topico));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TopicoDtoDetails> getTopico(@PathVariable final Long id) {
        Optional<Topico> topico = topicoService.getTopic(id);
        return topico.map(response -> ResponseEntity.ok(new TopicoDtoDetails(response)))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<TopicoDto> updateTopico(@PathVariable final Long id, @RequestBody @Valid TopicoBasicForm form) {
        Optional<Topico> topico = topicoService.getTopic(id);

        return topico.map(response -> {
            response.setTitulo(form.getTitulo());
            response.setMensagem(form.getMensagem());
            return ResponseEntity.ok(new TopicoDto(response));
        }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @DeleteMapping("{id}")
    @Transactional
    public ResponseEntity deleteTopic(@PathVariable final Long id) {
        Optional<Topico> topico = topicoService.getTopic(id);
        if (topico.isPresent()) {
            topicoService.delete(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();

    }

}
