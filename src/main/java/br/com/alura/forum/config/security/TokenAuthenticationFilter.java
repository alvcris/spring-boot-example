package br.com.alura.forum.config.security;

import br.com.alura.forum.modelo.Usuario;
import br.com.alura.forum.service.TokenService;
import br.com.alura.forum.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Getter
@AllArgsConstructor
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private TokenService tokenService;
    private UserService userService;

    @Override
    protected void doFilterInternal(
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final FilterChain filterChain) throws ServletException, IOException {

        String token = retrieveTokenFromRequest(httpServletRequest);
        if (tokenService.isTokenValid(token)) {
            authenticateClient(token);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private void authenticateClient(final String token) {
        Long userId = tokenService.getIdFromToken(token);
        Optional<Usuario> user = userService.findById(userId);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user, null, user.get().getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    private String retrieveTokenFromRequest(final HttpServletRequest httpServletRequest) {
        final int TOKEN_START_INDEX = 7;

        String token = httpServletRequest.getHeader("Authorization");

        if (StringUtils.isEmpty(token) || !StringUtils.startsWithIgnoreCase(token, "bearer ")) {
            return null;
        }
        return token.substring(TOKEN_START_INDEX);
    }
}
