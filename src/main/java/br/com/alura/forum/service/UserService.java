package br.com.alura.forum.service;

import br.com.alura.forum.modelo.Usuario;
import br.com.alura.forum.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Optional<Usuario> findById(final Long userId) {
        return userRepository.findById(userId);
    }

    public Optional<Usuario> findByEmail(final String userName) {
        return userRepository.findByEmail(userName);
    }
}
