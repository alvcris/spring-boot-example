package br.com.alura.forum.service;

import br.com.alura.forum.modelo.Topico;
import br.com.alura.forum.repository.TopicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TopicoService {

    @Autowired
    TopicoRepository topicoRepository;


    public Page<Topico> getAllTopics(final Pageable pageable) {
        return topicoRepository.findAll(pageable);
    }

    public void save(final Topico topico) {
        topicoRepository.save(topico);
    }

    public Optional<Topico> getTopic(final Long id) {
        return topicoRepository.findById(id);
    }

    public void delete(final Long id) {
        topicoRepository.deleteById(id);
    }
}
